import java.time.LocalDate;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;

public class Main {
	public static void main(String[] args) {
		Scanner myObj = new Scanner(System.in);
		System.out.println("Input real world date (format DD/MM/YYYY)");
		String realDate = myObj.nextLine();
		myObj.close();
		LocalDate real;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		real = LocalDate.parse(realDate, formatter);
		ImperialDate.convertToImpDate(real);
	}
}

import java.time.LocalDate;
import java.util.ArrayList;

public class ImperialDate {
	
	static double earthOrbitalPeriod = 365;
	static double dromundKaasOrbitalPeriod = 312;
	static double orbitalConversion = dromundKaasOrbitalPeriod/earthOrbitalPeriod;
	
	public static int getRealDayOfYear(LocalDate real) {
		int realDayOfYear = real.getDayOfYear();
		return realDayOfYear;
	}
	public static int getImpDayOfYear(LocalDate real) {
		double realDayOfYear = real.getDayOfYear();
		double realDayDouble = realDayOfYear;
		double impDayOfYear = orbitalConversion*realDayDouble;
		return (int)Math.round(impDayOfYear);
	}
	public static String getImpYear(LocalDate real) {
		int realYear = real.getYear();
		int impYear = realYear-1994;
		String impYearString;
		if (impYear >0) {
			impYearString = Integer.toString(impYear) + "ATC";
			return impYearString;
		}
		else {
			impYear *= -1;
			impYearString = Integer.toString(impYear) + "BTC";
			return impYearString;
		}
	}
	
	public static void convertToImpDate(LocalDate real) {
		int impDayOfYear = ImperialDate.getImpDayOfYear(real);
		String impYear = ImperialDate.getImpYear(real);
		ArrayList<String> impDays = new ArrayList<String>();
		for(int i = 0; i<=12; i++) {
			impDays.add("Primeday");
			impDays.add("Centaxday");
			impDays.add("Taungsday");
			impDays.add("Zhellday");
			impDays.add("Benduday");
		}
		impDays.add("Empire Day");
		for(int i = 0; i<=30; i++) {
			impDays.add("Primeday");
			impDays.add("Centaxday");
			impDays.add("Taungsday");
			impDays.add("Zhellday");
			impDays.add("Benduday");
		}
		impDays.add("Kaas Day");
		for(int i = 0; i<=17; i++) {
			impDays.add("Primeday");
			impDays.add("Centaxday");
			impDays.add("Taungsday");
			impDays.add("Zhellday");
			impDays.add("Benduday");
		}
		String impDate;
		String impDayOfWeek;
		String impMonth;
		impDayOfWeek = impDays.get(impDayOfYear-1).toString();
		if (impDayOfYear > 0 && impDayOfYear <= 35) {
			impMonth = "01";
			int dateInt = impDayOfYear;
			impDate = String.format("%02d", dateInt);
			if (impDayOfYear > 0 && impDayOfYear <=5) {
				System.out.println("Imperial (Kaas Calendar) Date: " + impDayOfWeek + ", " + impDate + "-" + impMonth + "-" + impYear + " (New Year's Fete Week)");
			}
			else {
				System.out.println("Imperial (Kaas Calendar) Date: " + impDayOfWeek + ", " + impDate + "-" + impMonth + "-" + impYear);
			}
		}
		else if (impDayOfYear > 35 && impDayOfYear <= 66) {
			impMonth = "02";
			int dateInt = (impDayOfYear-35);
			impDate = String.format("%02d", dateInt);
			System.out.println("Imperial (Kaas Calendar) Date: " + impDayOfWeek + ", " + impDate + "-" + impMonth + "-" + impYear);
		}
		else if (impDayOfYear > 66 && impDayOfYear <= 96) {
			impMonth = "03";
			int dateInt = (impDayOfYear-66);
			impDate = String.format("%02d", dateInt);
			System.out.println("Imperial (Kaas Calendar) Date: " + impDayOfWeek + ", " + impDate + "-" + impMonth + "-" + impYear);
		}
		else if (impDayOfYear > 96 && impDayOfYear <= 126) {
			impMonth = "04";
			int dateInt = (impDayOfYear-96);
			impDate = String.format("%02d", dateInt);
			System.out.println("Imperial (Kaas Calendar) Date: " + impDayOfWeek + ", " + impDate + "-" + impMonth + "-" + impYear);
		}
		else if (impDayOfYear > 126 && impDayOfYear <= 161) {
			impMonth = "05";
			int dateInt = (impDayOfYear-126);
			impDate = String.format("%02d", dateInt);
			if (impDayOfYear > 156 && impDayOfYear <=161) {
				System.out.println("Imperial (Kaas Calendar) Date: " + impDayOfWeek + ", " + impDate + "-" + impMonth + "-" + impYear + " (Fete Week 2)");
			}
			else {
				System.out.println("Imperial (Kaas Calendar) Date: " + impDayOfWeek + ", " + impDate + "-" + impMonth + "-" + impYear);
			}
		}
		else if (impDayOfYear > 161 && impDayOfYear <= 191) {
			impMonth = "06";
			int dateInt = (impDayOfYear-161);
			impDate = String.format("%02d", dateInt);
			System.out.println("Imperial (Kaas Calendar) Date: " + impDayOfWeek + ", " + impDate + "-" + impMonth + "-" + impYear);
		}
		else if (impDayOfYear > 191 && impDayOfYear <= 222) {
			impMonth = "07";
			int dateInt = (impDayOfYear-191);
			impDate = String.format("%02d", dateInt);
			System.out.println("Imperial (Kaas Calendar) Date: " + impDayOfWeek + ", " + impDate + "-" + impMonth + "-" + impYear);
		}
		else if (impDayOfYear > 222 && impDayOfYear <= 252) {
			impMonth = "08";
			int dateInt = (impDayOfYear-222);
			impDate = String.format("%02d", dateInt);
			System.out.println("Imperial (Kaas Calendar) Date: " + impDayOfWeek + ", " + impDate + "-" + impMonth + "-" + impYear);
		}
		else if (impDayOfYear > 252 && impDayOfYear <= 282) {
			impMonth = "09";
			int dateInt = (impDayOfYear-252);
			impDate = String.format("%02d", dateInt);
			System.out.println("Imperial (Kaas Calendar) Date: " + impDayOfWeek + ", " + impDate + "-" + impMonth + "-" + impYear);
		}
		else if (impDayOfYear > 282 && impDayOfYear <= 312) {
			impMonth = "10";
			int dateInt = (impDayOfYear-282);
			impDate = String.format("%02d", dateInt);
			System.out.println("Imperial (Kaas Calendar) Date: " + impDayOfWeek + ", " + impDate + "-" + impMonth + "-" + impYear);
		}
	}
}
